/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_lstdelfirst.c                                   :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mvladymy <mvladymy@student.unit.ua>        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/02/14 15:35:41 by mvladymy          #+#    #+#             */
/*   Updated: 2018/02/14 15:38:47 by mvladymy         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdlib.h>
#include "libft.h"

void	ft_lstdelfirst(t_list **alst)
{
	t_list	*first_elem;

	first_elem = *alst;
	*alst = (*alst)->next;
	free(first_elem->content);
	free(first_elem);
}
