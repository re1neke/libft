/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_memcpy.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mvladymy <mvladymy@student.unit.ua>        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/10/26 13:12:53 by mvladymy          #+#    #+#             */
/*   Updated: 2017/11/04 18:38:20 by mvladymy         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdint.h>
#include "libft.h"

void	*ft_memcpy(void *dst, const void *src, size_t n)
{
	uint8_t *u_dst;
	uint8_t	*u_src;

	u_dst = (uint8_t *)dst;
	u_src = (uint8_t *)src;
	while (n--)
		u_dst[n] = u_src[n];
	return (u_dst);
}
