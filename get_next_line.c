/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   get_next_line.c                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mvladymy <mvladymy@student.unit.ua>        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/12/13 12:13:29 by mvladymy          #+#    #+#             */
/*   Updated: 2018/05/12 13:56:34 by mvladymy         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <unistd.h>
#include <stdlib.h>
#include <libft.h>

static t_list	*get_file(int fd)
{
	t_list			*tmp;
	static t_list	*head;

	tmp = head;
	while (tmp)
	{
		if ((int)tmp->content_size == fd)
			return (tmp);
		tmp = tmp->next;
	}
	ft_lstadd(&head, ft_lstnew(NULL, sizeof(t_list *)));
	head->content_size = (size_t)fd;
	return (head);
}

static char		*to_line(t_list **buff_list)
{
	t_list	*tmp_list;
	size_t	line_len;
	char	*line;

	line_len = 0;
	tmp_list = *buff_list;
	while (tmp_list && !ft_strequ(tmp_list->content, "\n"))
	{
		line_len += tmp_list->content_size - 1;
		tmp_list = tmp_list->next;
	}
	if (!(line = ft_strnew(line_len)))
		return (NULL);
	while (*buff_list)
	{
		if (ft_strequ((*buff_list)->content, "\n"))
		{
			ft_lstdelfirst(buff_list);
			break ;
		}
		ft_strcat(line, (*buff_list)->content);
		ft_lstdelfirst(buff_list);
	}
	return (line);
}

static t_list	*split_buff(char *start)
{
	char	*end;
	t_list	*start_list;
	t_list	*elem;

	if (!*start)
		return (NULL);
	start_list = NULL;
	while ((end = ft_strchr(start, '\n')))
	{
		if (end - start)
		{
			elem = ft_lstnew("\0", (size_t)(end - start + 1));
			ft_strncat((char *)elem->content, start, (size_t)(end - start));
			ft_lstradd(&start_list, elem);
		}
		ft_lstradd(&start_list, ft_lstnew("\n", 2));
		start = end + 1;
	}
	if (*start)
		ft_lstradd(&start_list, ft_lstnew(start, ft_strlen(start) + 1));
	return (start_list);
}

int				get_buff_line(const int fd, char **line)
{
	t_list	*cur_file;

	cur_file = get_file(fd);
	if (!cur_file->content)
		return (0);
	if (!(*line = to_line((t_list **)&cur_file->content)))
		return (-1);
	return (1);
}

int				get_next_line(const int fd, char **line)
{
	char			buff[GNL_BUFF_SIZE + 1];
	t_list			*cur_file;
	t_list			**last_elem;
	ssize_t			readen;

	cur_file = get_file(fd);
	if (!cur_file || fd < 0 || GNL_BUFF_SIZE < 1 || read(fd, buff, 0) < 0 ||
			!line)
		return (-1);
	last_elem = (t_list **)&cur_file->content;
	while ((readen = read(fd, buff, GNL_BUFF_SIZE)) && !ft_strchr(buff, '\n'))
	{
		buff[readen] = '\0';
		last_elem = ft_lstradd(last_elem, ft_lstnew(buff,
			(size_t)(readen + 1)));
		if (!last_elem)
			return (-1);
	}
	buff[readen] = '\0';
	if (!cur_file->content && ft_strlen(buff) == 0)
		return (0);
	ft_lstradd(last_elem, split_buff(buff));
	if (!(*line = to_line((t_list **)&cur_file->content)))
		return (-1);
	return (1);
}
