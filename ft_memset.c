/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_memset.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mvladymy <mvladymy@student.unit.ua>        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/10/26 12:58:27 by mvladymy          #+#    #+#             */
/*   Updated: 2017/11/04 19:25:48 by mvladymy         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdint.h>
#include "libft.h"

void	*ft_memset(void *b, int c, size_t len)
{
	uint8_t	*b_c;
	uint8_t	c_c;

	c_c = (uint8_t)c;
	b_c = (uint8_t *)b;
	while (len--)
		b_c[len] = c_c;
	return (b_c);
}
