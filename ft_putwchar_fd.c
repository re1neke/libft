/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_putwchar_fd.c                                   :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mvladymy <mvladymy@student.unit.ua>        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/08/01 21:54:39 by mvladymy          #+#    #+#             */
/*   Updated: 2018/08/01 21:54:56 by mvladymy         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdint.h>
#include <unistd.h>
#include <stdlib.h>
#include <libft.h>

static inline void	w8bit(wchar_t ch, int fd)
{
	uint8_t	tmp_ch[1];

	tmp_ch[0] = (uint8_t)ch;
	write(fd, tmp_ch, 1);
}

static inline void	w16bit(wchar_t ch, int fd)
{
	uint8_t	tmp_ch[2];

	tmp_ch[0] = (uint8_t)(((ch >> 6) & 0x1F) | 0xC0);
	tmp_ch[1] = (uint8_t)((ch & 0x3F) | 0x80);
	write(fd, tmp_ch, 2);
}

static inline void	w24bit(wchar_t ch, int fd)
{
	uint8_t	tmp_ch[3];

	tmp_ch[0] = (uint8_t)(((ch >> 12) & 0xF) | 0xE0);
	tmp_ch[1] = (uint8_t)(((ch >> 6) & 0x3F) | 0x80);
	tmp_ch[2] = (uint8_t)((ch & 0x3F) | 0x80);
	write(fd, tmp_ch, 3);
}

static inline void	w32bit(wchar_t ch, int fd)
{
	uint8_t	tmp_ch[4];

	tmp_ch[0] = (uint8_t)(((ch >> 18) & 0x7) | 0xF0);
	tmp_ch[1] = (uint8_t)(((ch >> 12) & 0x3F) | 0x80);
	tmp_ch[2] = (uint8_t)(((ch >> 6) & 0x3F) | 0x80);
	tmp_ch[3] = (uint8_t)((ch & 0x3F) | 0x80);
	write(fd, tmp_ch, 4);
}

void				ft_putwchar_fd(wchar_t ch, int fd)
{
	if (ch <= 0x7F || MB_CUR_MAX == 1)
		w8bit(ch, fd);
	else if ((ch <= 0x7FF && ch > 0x7F) || MB_CUR_MAX == 2)
		w16bit(ch, fd);
	else if ((ch <= 0xFFFF && ch > 0x7FF) || MB_CUR_MAX == 3)
		w24bit(ch, fd);
	else
		w32bit(ch, fd);
}
