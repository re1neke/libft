/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strjoin.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mvladymy <mvladymy@student.unit.ua>        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/10/26 12:10:37 by mvladymy          #+#    #+#             */
/*   Updated: 2017/11/04 16:04:53 by mvladymy         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

char	*ft_strjoin(const char *s1, const char *s2)
{
	char	*fresh;
	size_t	len;
	size_t	index;
	size_t	l_index;

	if (!s1 || !s2)
		return (NULL);
	len = ft_strlen(s1) + ft_strlen(s2);
	fresh = ft_strnew(len);
	if (len && fresh)
	{
		index = 0;
		l_index = 0;
		while (s1[l_index] != '\0')
			fresh[index++] = s1[l_index++];
		l_index = 0;
		while (s2[l_index] != '\0')
			fresh[index++] = s2[l_index++];
		fresh[index] = '\0';
	}
	return (fresh);
}
