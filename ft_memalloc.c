/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_memalloc.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mvladymy <mvladymy@student.unit.ua>        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/10/26 09:12:35 by mvladymy          #+#    #+#             */
/*   Updated: 2017/11/05 15:14:09 by mvladymy         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdint.h>
#include <stdlib.h>
#include "libft.h"

void	*ft_memalloc(size_t size)
{
	uint8_t	*area;

	area = malloc(size);
	if (!area)
		return (NULL);
	while (size--)
		area[size] = 0;
	return (area);
}
