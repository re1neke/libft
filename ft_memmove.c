/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_memmove.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mvladymy <mvladymy@student.unit.ua>        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/11/02 14:39:45 by mvladymy          #+#    #+#             */
/*   Updated: 2017/11/07 15:44:38 by mvladymy         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdint.h>
#include "libft.h"

void	*ft_memmove(void *dst, const void *src, size_t len)
{
	size_t	index;
	uint8_t	*u_dst;
	uint8_t	*u_src;

	u_dst = (uint8_t *)dst;
	u_src = (uint8_t *)src;
	if ((u_src + len) > u_dst && u_src < u_dst)
		ft_memcpy(dst, src, len);
	else
	{
		index = 0;
		while (index++ < len)
			u_dst[index - 1] = u_src[index - 1];
	}
	return (u_dst);
}
