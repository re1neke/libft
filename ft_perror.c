/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_perror.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mvladymy <mvladymy@student.unit.ua>        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/08/28 14:20:57 by mvladymy          #+#    #+#             */
/*   Updated: 2018/08/28 14:21:51 by mvladymy         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <errno.h>
#include <unistd.h>
#include <libft.h>

void	ft_perror(const char *s)
{
	ft_dprintf(STDERR_FILENO, "%s: %s\n", s, strerror(errno));
}
