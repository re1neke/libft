/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strtrim.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mvladymy <mvladymy@student.unit.ua>        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/10/29 15:34:26 by mvladymy          #+#    #+#             */
/*   Updated: 2017/11/09 15:48:42 by mvladymy         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

char		*ft_strtrim(const char *s)
{
	unsigned int	begin;
	unsigned int	end;
	unsigned int	index;

	if (!s)
		return (NULL);
	begin = 0;
	while (ft_iswhitespace(s[begin]) && s[begin] != '\0')
		begin++;
	index = begin;
	end = begin;
	while (s[index++] != '\0')
	{
		if (!ft_iswhitespace(s[index - 1]) &&
				(ft_iswhitespace(s[index]) || s[index] == '\0'))
			end = index;
	}
	return (ft_strsub(s, begin, end - begin));
}
