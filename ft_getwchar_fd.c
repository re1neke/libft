/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_getwchar_fd.c                                   :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mvladymy <mvladymy@student.unit.ua>        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/08/09 16:35:28 by mvladymy          #+#    #+#             */
/*   Updated: 2018/08/09 16:35:28 by mvladymy         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <unistd.h>
#include "libft.h"

static inline wchar_t	r16bit(char *buf, int fd)
{
	read(fd, &buf[1], 1);
	return (((buf[0] & ~0xE0) << 6) + (buf[1] & ~0xC0));
}

static inline wchar_t	r24bit(char *buf, int fd)
{
	read(fd, &buf[1], 2);
	return (((buf[0] & ~0xF0) << 12)
			+ ((buf[1] & ~0xC0) << 6)
			+ (buf[2] & ~0xC0));
}

static inline wchar_t	r32bit(char *buf, int fd)
{
	read(fd, &buf[1], 3);
	return (((buf[0] & ~0xF8) << 18)
			+ ((buf[1] & ~0xC0) << 12)
			+ ((buf[2] & ~0xC0) << 6)
			+ ((buf[3] & ~0xC0)));
}

wchar_t					ft_getwchar_fd(int fd)
{
	char	buf[4];
	wchar_t	ch;

	read(fd, &buf[0], 1);
	if ((buf[0] & 0xE0) == 0xC0)
		ch = r16bit(buf, fd);
	else if ((buf[0] & 0xF0) == 0xE0)
		ch = r24bit(buf, fd);
	else if ((buf[0] & 0xF8) == 0xF0)
		ch = r32bit(buf, fd);
	else
		ch = buf[0];
	return (ch);
}
