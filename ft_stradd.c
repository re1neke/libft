/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_stradd.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mvladymy <mvladymy@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/03/15 18:18:25 by mvladymy          #+#    #+#             */
/*   Updated: 2018/04/18 14:58:48 by mvladymy         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdlib.h>
#include "libft.h"

char	*ft_stradd(char **dst, const char *src)
{
	char	*new_line;

	if (!*dst)
	{
		*dst = ft_strdup((char *)src);
		return (*dst);
	}
	else if (!src)
		return (*dst);
	if (!(new_line = ft_strjoin(*dst, src)))
		return (*dst);
	free(*dst);
	*dst = new_line;
	return (new_line);
}
