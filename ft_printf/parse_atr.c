/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   parse_atr.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mvladymy <mvladymy@student.unit.ua>        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/02/09 12:25:51 by mvladymy          #+#    #+#             */
/*   Updated: 2018/05/12 14:37:25 by mvladymy         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <ft_printf.h>

static bool		is_conv_flag_char(char c)
{
	if (c == '#' ||
			c == '+' || c == '-' || c == ' ' || c == '0' ||
			c == 'h' || c == 'l' || c == 'j' || c == 'z')
		return (true);
	else
		return (false);
}

static size_t	parse_flags(char *conv_str, t_conv *conv_atr)
{
	size_t	pos;

	pos = 0;
	while (is_conv_flag_char(conv_str[pos]))
	{
		if (conv_str[pos] == 'l' && conv_atr->conv_flags & CONV_FLAG_LONG)
			conv_atr->conv_flags |= CONV_FLAG_LLONG;
		if (conv_str[pos] == 'h' && conv_atr->conv_flags & CONV_FLAG_SHORT)
			conv_atr->conv_flags |= CONV_FLAG_CHAR;
		conv_atr->conv_flags |= (conv_str[pos] == '#') ? CONV_FLAG_HASH : 0;
		conv_atr->conv_flags |= (conv_str[pos] == '0') ? CONV_FLAG_ZERO : 0;
		conv_atr->conv_flags |= (conv_str[pos] == '-') ? CONV_FLAG_NEG : 0;
		conv_atr->conv_flags |= (conv_str[pos] == '+') ? CONV_FLAG_POS : 0;
		conv_atr->conv_flags |= (conv_str[pos] == ' ') ? CONV_FLAG_SPACE : 0;
		conv_atr->conv_flags |= (conv_str[pos] == 'h') ? CONV_FLAG_SHORT : 0;
		conv_atr->conv_flags |= (conv_str[pos] == 'l') ? CONV_FLAG_LONG : 0;
		conv_atr->conv_flags |= (conv_str[pos] == 'j') ? CONV_FLAG_IMAX : 0;
		conv_atr->conv_flags |= (conv_str[pos] == 'z') ? CONV_FLAG_SIZET : 0;
		pos++;
	}
	return (pos);
}

static void		read_asterisk(va_list *ap, t_conv *conv_atr, uint8_t flag)
{
	int	value;

	value = va_arg(*ap, int);
	if (flag == RA_WIDTH && value >= 0)
		conv_atr->conv_width = value;
	else if (flag == RA_PRECS && value >= 0)
		conv_atr->conv_precision = value;
	else if (flag == RA_WIDTH && value < 0)
	{
		conv_atr->conv_width = -value;
		conv_atr->conv_flags |= CONV_FLAG_NEG;
	}
	else if (flag == RA_PRECS && value < 0)
		conv_atr->conv_flags &= ~CONV_FLAG_PRECS;
}

size_t			parse_atr(va_list *ap, char *conv_str, t_conv *conv_atr)
{
	size_t	pos;

	pos = parse_flags(conv_str, conv_atr);
	if (ft_isdigit(conv_str[pos]))
		conv_atr->conv_width = (int)ft_atoi(&conv_str[pos]);
	ITERATE_UNTIL(ft_isdigit(conv_str[pos]), pos);
	if (conv_str[pos] == '*' && ++pos)
		read_asterisk(ap, conv_atr, RA_WIDTH);
	if (ft_isdigit(conv_str[pos]))
		conv_atr->conv_width = (int)ft_atoi(&conv_str[pos]);
	ITERATE_UNTIL(ft_isdigit(conv_str[pos]) || conv_str[pos] == '*', pos);
	pos += parse_flags(&conv_str[pos], conv_atr);
	if (conv_str[pos] == '.' && ++pos)
		conv_atr->conv_flags |= CONV_FLAG_PRECS;
	if (ft_isdigit(conv_str[pos]))
		conv_atr->conv_precision = (int)ft_atoi(&conv_str[pos]);
	ITERATE_UNTIL(ft_isdigit(conv_str[pos]) || conv_str[pos] == '.', pos);
	if (conv_str[pos] == '*')
		read_asterisk(ap, conv_atr, RA_PRECS);
	ITERATE_UNTIL(ft_isdigit(conv_str[pos]) || conv_str[pos] == '.' ||
			conv_str[pos] == '*', pos);
	pos += parse_flags(&conv_str[pos], conv_atr);
	return (pos);
}
