/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   read_num.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mvladymy <mvladymy@student.unit.ua>        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/02/06 14:54:32 by mvladymy          #+#    #+#             */
/*   Updated: 2018/02/21 14:01:32 by mvladymy         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <ft_printf.h>

static void		put_number(size_t num, uint8_t base, bool upper_case)
{
	size_t	tmp_num;
	size_t	ratio;
	size_t	tmp_char;
	uint8_t	l_case;

	ratio = 1;
	tmp_num = num;
	while ((tmp_num = tmp_num / base))
		ratio *= base;
	l_case = (upper_case) ? 'A' : 'a';
	while (ratio)
	{
		tmp_char = num / ratio;
		tmp_char += (tmp_char < 10) ? '0' : l_case - 10;
		PUSH_TO_BUFFER(tmp_char);
		num %= ratio;
		ratio /= base;
	}
}

static void		add_hash(t_conv *atr, bool num)
{
	if ((atr->conv_type == 'O' || atr->conv_type == 'o') && num)
		PUSH_TO_BUFFER('0');
	else if ((atr->conv_type == 'x' && num) || atr->conv_type == 'p')
		push_line_to_buffer("0x");
	else if (atr->conv_type == 'X' && num)
		push_line_to_buffer("0X");
	else if ((atr->conv_type == 'b' && num))
		push_line_to_buffer("0b");
}

static void		put_sign(t_conv *atr, bool num, bool neg)
{
	if ((atr->conv_flags & CONV_FLAG_HASH &&
			(atr->conv_type == 'o' || atr->conv_type == 'O' ||
			atr->conv_type == 'x' || atr->conv_type == 'X' ||
			atr->conv_type == 'b')) ||
			atr->conv_type == 'p')
		add_hash(atr, num);
	else if (neg)
		PUSH_TO_BUFFER('-');
	else if (atr->conv_type != 'o' && atr->conv_type != 'O' &&
			atr->conv_type != 'u' && atr->conv_type != 'U' &&
			atr->conv_type != 'x' && atr->conv_type != 'X' &&
			atr->conv_type != 'b')
	{
		if (atr->conv_flags & CONV_FLAG_POS)
			PUSH_TO_BUFFER('+');
		else if (atr->conv_flags & CONV_FLAG_SPACE)
			PUSH_TO_BUFFER(' ');
	}
}

static uint8_t	count_sign(t_conv *atr, bool num, bool neg)
{
	if (!atr->conv_width ||
			atr->conv_type == 'u' || atr->conv_type == 'U' ||
			(!(atr->conv_flags & CONV_FLAG_HASH) &&
			(atr->conv_type == 'o' || atr->conv_type == 'O' ||
			atr->conv_type == 'x' || atr->conv_type == 'X' ||
			atr->conv_type == 'X')))
		return (0);
	if (atr->conv_type == 'p' ||
			(num && (atr->conv_type == 'X' || atr->conv_type == 'x' ||
			atr->conv_type == 'b') &&
			atr->conv_flags & CONV_FLAG_HASH))
		return (2);
	else if ((neg ||
			atr->conv_flags & CONV_FLAG_SPACE ||
			atr->conv_flags & CONV_FLAG_POS) &&
			atr->conv_type != 'o' && atr->conv_type != 'O' &&
			atr->conv_type != 'x' && atr->conv_type != 'X' &&
			atr->conv_type != 'b')
		return (1);
	else
		return (0);
}

void			read_num(size_t num, t_conv *atr, uint8_t base, bool neg)
{
	uint8_t	num_l;
	uint8_t	sign;

	num_l = ft_count_digits(num, base);
	num_l += ((atr->conv_type == 'o' || atr->conv_type == 'O') && num &&
			atr->conv_flags & CONV_FLAG_HASH) ? 1 : 0;
	sign = count_sign(atr, !!num, neg);
	if (!(atr->conv_flags & CONV_FLAG_NEG) &&
			(!(atr->conv_flags & CONV_FLAG_ZERO) ||
			(atr->conv_flags & CONV_FLAG_PRECS)))
		SPACE_PADDING(atr->conv_width - sign, MAX(atr->conv_precision, num_l));
	put_sign(atr, !!num, neg);
	if (atr->conv_flags & CONV_FLAG_PRECS)
		ZERO_PADDING(atr->conv_precision, num_l);
	else if (atr->conv_flags & CONV_FLAG_ZERO &&
			!(atr->conv_flags & CONV_FLAG_NEG))
		ZERO_PADDING(atr->conv_width - sign, MAX(atr->conv_precision, num_l));
	if (!((atr->conv_flags & CONV_FLAG_PRECS) && !atr->conv_precision) || num ||
			((atr->conv_type == 'o' || atr->conv_type == 'O') &&
			atr->conv_flags & CONV_FLAG_HASH))
		put_number(num, base, atr->conv_type == 'X');
	else if (!num && atr->conv_width)
		PUSH_TO_BUFFER(' ');
	if (atr->conv_flags & CONV_FLAG_NEG)
		SPACE_PADDING(atr->conv_width - sign, MAX(atr->conv_precision, num_l));
}
