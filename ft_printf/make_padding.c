/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   make_padding.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mvladymy <mvladymy@student.unit.ua>        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/02/07 13:49:46 by mvladymy          #+#    #+#             */
/*   Updated: 2018/02/16 15:51:05 by mvladymy         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <ft_printf.h>

void			make_padding(unsigned int conv_width, unsigned int len,
						char filler)
{
	unsigned int	padding;

	padding = (conv_width > len) ? conv_width - len : 0;
	while (padding--)
		PUSH_TO_BUFFER(filler);
}
