/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   read_unicode.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mvladymy <mvladymy@student.unit.ua>        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/02/14 17:37:28 by mvladymy          #+#    #+#             */
/*   Updated: 2018/02/17 17:59:58 by mvladymy         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <ft_printf.h>

static char		bytes_count(wchar_t ch)
{
	unsigned int	u_ch;

	u_ch = (unsigned int)ch;
	if (u_ch <= 0b1111111 || MB_CUR_MAX == 1)
		return (1);
	else if ((u_ch <= 0b11111111111 && u_ch > 0b1111111) || MB_CUR_MAX == 2)
		return (2);
	else if ((u_ch <= 0b1111111111111111 && u_ch > 0b11111111111)
			|| MB_CUR_MAX == 3)
		return (3);
	else
		return (4);
}

static size_t	unistr_len(wchar_t *str, t_conv *atr)
{
	size_t			len;
	unsigned char	char_bytes;

	len = 0;
	while (*str)
	{
		char_bytes = bytes_count(*str++);
		if (len + char_bytes > (unsigned int)atr->conv_precision &&
				atr->conv_flags & CONV_FLAG_PRECS)
			break ;
		len += char_bytes;
	}
	return (len);
}

static void		cast_unicode(wchar_t ch)
{
	unsigned int	u_ch;

	u_ch = (unsigned int)ch;
	if (u_ch <= 0b1111111 || MB_CUR_MAX == 1)
		PUSH_TO_BUFFER((char)u_ch);
	else if ((u_ch <= 0b11111111111 && u_ch > 0b1111111) || MB_CUR_MAX == 2)
	{
		PUSH_TO_BUFFER((char)((((u_ch >> 6) << 27) >> 27) | 0b11000000));
		PUSH_TO_BUFFER((char)(((u_ch << 26) >> 26) | 0b10000000));
	}
	else if ((u_ch <= 0b1111111111111111 && u_ch > 0b11111111111)
			|| MB_CUR_MAX == 3)
	{
		PUSH_TO_BUFFER((char)((((u_ch >> 12) << 28) >> 28) | 0b11100000));
		PUSH_TO_BUFFER((char)((((u_ch >> 6) << 26) >> 26) | 0b10000000));
		PUSH_TO_BUFFER((char)(((u_ch << 26) >> 26) | 0b10000000));
	}
	else
	{
		PUSH_TO_BUFFER((char)((((u_ch >> 18) << 29) >> 29) | 0b11110000));
		PUSH_TO_BUFFER((char)((((u_ch >> 12) << 26) >> 26) | 0b10000000));
		PUSH_TO_BUFFER((char)((((u_ch >> 6) << 26) >> 26) | 0b10000000));
		PUSH_TO_BUFFER((char)(((u_ch << 26) >> 26) | 0b10000000));
	}
}

void			read_unistr(wchar_t *str, t_conv *atr)
{
	size_t			str_len;
	size_t			printed_len;

	str = (str != NULL) ? str : L"(null)";
	if (atr->conv_flags & CONV_FLAG_PRECS)
	{
		str_len = unistr_len(str, atr);
		printed_len = atr->conv_precision;
	}
	else
	{
		str_len = unistr_len(str, atr);
		printed_len = str_len;
	}
	if (!(atr->conv_flags & CONV_FLAG_NEG) && atr->conv_flags & CONV_FLAG_ZERO)
		ZERO_PADDING(atr->conv_width, str_len);
	else if (!(atr->conv_flags & CONV_FLAG_NEG))
		SPACE_PADDING(atr->conv_width, str_len);
	while (*str && printed_len >= (size_t)bytes_count(*str))
	{
		cast_unicode(*str);
		printed_len -= bytes_count(*str++);
	}
	if (atr->conv_flags & CONV_FLAG_NEG)
		SPACE_PADDING(atr->conv_width, str_len);
}

void			read_unichar(wchar_t ch, t_conv *atr)
{
	char	width;

	width = bytes_count(ch);
	if (!(atr->conv_flags & CONV_FLAG_NEG))
	{
		if (atr->conv_flags & CONV_FLAG_ZERO)
			ZERO_PADDING(atr->conv_width, width);
		else
			SPACE_PADDING(atr->conv_width, width);
	}
	cast_unicode(ch);
	if (atr->conv_flags & CONV_FLAG_NEG)
		SPACE_PADDING(atr->conv_width, width);
}
